const fs = require("fs");
const superagent = require("superagent");

let readFilePromise = (file) => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, (err, data) => {
      if (err) reject("not able to read the file");
      resolve(data);
    });
  });
};

let writeFilePromise = (data, file) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, (err) => {
      if (err) reject("not able to write content to the file");
      resolve();
    });
  });
};

// logic using async await
const getDog = async () => {
  try {
    const data = await readFilePromise("./dogg.txt");
    console.log(`breed is ${data}`);

    const res = await superagent.get(
      `https://dog.ceo/api/breed/${data}/images/random`
    );
    console.log(res.body.message);
    await writeFilePromise(res.body.message, "dog-data.txt");
    console.log("sucessfully stored the data to the file");
  } catch (err) {
    console.log(err);
    throw err;
  }
  return "2. running ";
};

//using async and await
(async () => {
  try {
    console.log("1. start");
    const x = await getDog();
    console.log(x);
    console.log("3. end");
  } catch (err) {
    console.log("ERROR!!!");
  }
})();

//using promises

// console.log("1. start");
// getDog()
//   .then((res) => {
//     console.log(res);
//     console.log("3. end");
//   })
//   .catch((err) => {
//     console.log("ERROR!!!");
//   });

//promise chaining

// readFilePromise("./dog.txt")
//   .then((data) => {
//     console.log(`breed is ${data}`);
//     return superagent.get(`https://dog.ceo/api/breed/${data}/images/random`);
//   })
//   .then((res) => {
//     console.log(res.body.message);
//     return writeFilePromise(res.body.message, "dog-data.txt");
//   })
//   .then(() => {
//     console.log("sucessfully stored the data to the file");
//   })
//   .catch((err) => {
//     console.log(err);
//   });
